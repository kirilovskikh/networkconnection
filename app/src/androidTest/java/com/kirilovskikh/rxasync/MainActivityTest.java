package com.kirilovskikh.rxasync;

import android.test.ActivityInstrumentationTestCase2;

import com.kirilovskikh.rxasync.view.activity.MainActivity;

/**
 * Created by kirilovskikh on 26.02.15.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity activity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        activity = getActivity();
    }

    public void testDel() throws Exception {
        assertEquals(activity.calcDel(2, 0), 2);
    }

}
