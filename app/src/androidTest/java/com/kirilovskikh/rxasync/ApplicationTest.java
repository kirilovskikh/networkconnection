package com.kirilovskikh.rxasync;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    private App app;

    public ApplicationTest() {
        super(Application.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        app = (App) getContext().getApplicationContext();
    }

    public void testSum() {
        assertEquals(app.calcSum(2, 3), 5);
    }

}