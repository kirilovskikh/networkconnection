package com.kirilovskikh.rxasync.utils;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class PreferencesConstant {

    public static final String PREFERENCES_NAME = "preferences_name";
    public static final String CONTRIBUTOR_LAST_CACHE = "contributor_last_cache";

}
