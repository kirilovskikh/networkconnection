package com.kirilovskikh.rxasync.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class PreferencesManager {

    private static PreferencesManager sInstance;

    private SharedPreferences preferences;

    public static PreferencesManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }

        return sInstance;
    }

    private PreferencesManager(Context context) {
        preferences = context.getSharedPreferences(PreferencesConstant.PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public long getLongValue(String key) {
        return preferences.getLong(key, 0);
    }

    public void setLongKey(String key, long value) {
        SharedPreferences.Editor edit = preferences.edit();
        edit.putLong(key, value);
        edit.apply();
    }

}
