package com.kirilovskikh.rxasync.view.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.kirilovskikh.rxasync.R;
import com.kirilovskikh.rxasync.adapters.ContributorAdapter;
import com.kirilovskikh.rxasync.api.Api;
import com.kirilovskikh.rxasync.api.network.ContributorEvent;
import com.kirilovskikh.rxasync.api.network.responseModel.Contributor;
import com.kirilovskikh.rxasync.view.BaseActivity;

import java.util.List;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class NetworkActivity extends BaseActivity {

    @InjectView(R.id.progressBar)
    ProgressBar progressBar;

    @InjectView(R.id.listView)
    ListView listView;

    @InjectView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    public int getLayout() {
        return R.layout.cache_activity;
    }

    @Override
    public void createView(View view) {
        EventBus.getDefault().register(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

        progressBar.setVisibility(View.VISIBLE);
        Api.fetchContributorList(getContext(), "square", "retrofit");
    }

    public void onEvent(ContributorEvent event) {
        updateView(event.getList());
    }

    private void updateView(List<Contributor> contributors) {
        progressBar.setVisibility(View.GONE);

        ContributorAdapter adapter = new ContributorAdapter(getContext(), contributors);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

}
