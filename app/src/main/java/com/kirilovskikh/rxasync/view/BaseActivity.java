package com.kirilovskikh.rxasync.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import butterknife.ButterKnife;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public abstract class BaseActivity extends ActionBarActivity
        implements UiInterface {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());

        context = this;
        ButterKnife.inject(this);

        createView(null);
    }

    public Context getContext() {
        return context;
    }

}
