package com.kirilovskikh.rxasync.view.activity;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kirilovskikh.rxasync.R;
import com.kirilovskikh.rxasync.adapters.RecyclerAdapter;
import com.kirilovskikh.rxasync.api.Api;
import com.kirilovskikh.rxasync.api.db.RealmApi;
import com.kirilovskikh.rxasync.api.db.model.ContributorModel;
import com.kirilovskikh.rxasync.view.BaseActivity;

import butterknife.InjectView;
import io.realm.RealmResults;
import rx.functions.Action1;

public class CacheActivity extends BaseActivity
        implements RecyclerAdapter.OnItemClickListener {

    @InjectView(R.id.progressBar)
    ProgressBar progressBar;

//    @InjectView(R.id.listView)
//    ListView listView;

    @InjectView(R.id.recyclerView)
    RecyclerView recyclerView;

    private String owner;
    private String repo;

    @Override
    public int getLayout() {
        return R.layout.cache_activity;
    }

    @Override
    public void createView(View view) {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        owner = "square";
        repo = "retrofit";

        Api.getContributorList(getContext(), owner, repo)
                .subscribe(new Action1<RealmResults<ContributorModel>>() {
                    @Override
                    public void call(RealmResults<ContributorModel> contributorModels) {
                        RealmResults<ContributorModel> contributors = RealmApi.getContributors(getContext(), owner, repo);
                        updateView(contributors);
                    }
                });
    }

    private void updateView(RealmResults<ContributorModel> contributors) {
        progressBar.setVisibility(View.GONE);

//        ContributorModelAdapter adapter = new ContributorModelAdapter(contributors, getContext());
//        listView.setAdapter(adapter);

        RecyclerAdapter adapter = new RecyclerAdapter(getContext(), contributors, this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(View view, ContributorModel viewModel) {
        Toast.makeText(getContext(), viewModel.getLogin(), Toast.LENGTH_SHORT).show();
    }

}
