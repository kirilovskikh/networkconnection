package com.kirilovskikh.rxasync.view;

import android.view.View;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public interface UiInterface {

    public int getLayout();
    public void createView(View view);

}
