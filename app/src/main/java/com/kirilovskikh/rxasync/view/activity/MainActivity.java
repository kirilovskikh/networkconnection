package com.kirilovskikh.rxasync.view.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.kirilovskikh.rxasync.R;
import com.kirilovskikh.rxasync.view.BaseActivity;

import butterknife.OnClick;


public class MainActivity extends BaseActivity {

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void createView(View view) {

    }

    @OnClick(R.id.cache_button)
    void clickCacheButton() {
        startActivity(new Intent(getContext(), CacheActivity.class));
    }

    @OnClick(R.id.network_button)
    void clickNetworkButton() {
        startActivity(new Intent(getContext(), NetworkActivity.class));
    }

    public int calcDel(int a, int b) {
        return a - b;
    }


}
