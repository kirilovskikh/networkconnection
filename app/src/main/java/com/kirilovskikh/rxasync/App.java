package com.kirilovskikh.rxasync;

import android.app.Application;

import com.kirilovskikh.rxasync.utils.PreferencesManager;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        PreferencesManager.getInstance(getApplicationContext());
    }

    public int calcSum(int a, int b) {
        return a + b;
    }

}
