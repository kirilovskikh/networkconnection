package com.kirilovskikh.rxasync.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kirilovskikh.rxasync.R;
import com.kirilovskikh.rxasync.api.db.model.ContributorModel;


import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.RealmResults;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class ContributorModelAdapter extends BaseAdapter {

    private RealmResults<ContributorModel> list;
    private Context context;

    public ContributorModelAdapter(RealmResults<ContributorModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.contributor_list_view_item, parent, false);

            ViewHolder holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        ContributorModel model = list.get(position);

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.textView.setText(model.getLogin());

        return convertView;
    }

    static class ViewHolder {

        @InjectView(R.id.textView)
        TextView textView;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }

}
