package com.kirilovskikh.rxasync.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kirilovskikh.rxasync.R;
import com.kirilovskikh.rxasync.api.db.model.ContributorModel;

import java.util.List;


/**
 * Created by kirilovskikh on 26.02.15.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>
        implements View.OnClickListener  {

    private Context context;
    private List<ContributorModel> list;
    private OnItemClickListener onItemClickListener;

    public RecyclerAdapter(Context context, List<ContributorModel> list, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.contributor_list_view_item, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ContributorModel model = list.get(position);
        holder.textView.setText(model.getLogin());

        holder.itemView.setTag(model);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onClick(final View v) {
        if (onItemClickListener != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onItemClickListener.onItemClick(v, (ContributorModel) v.getTag());
                }
            }, 200);
        }

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        ViewHolder(View itemView) {
            super(itemView);

            textView = (TextView) itemView.findViewById(R.id.textView);
        }

    }

    public interface OnItemClickListener {

        void onItemClick(View view, ContributorModel viewModel);

    }


}
