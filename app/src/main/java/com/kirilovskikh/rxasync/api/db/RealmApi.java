package com.kirilovskikh.rxasync.api.db;

import android.content.Context;

import com.kirilovskikh.rxasync.api.db.model.ContributorModel;
import com.kirilovskikh.rxasync.api.network.responseModel.Contributor;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class RealmApi {

    public static RealmResults<ContributorModel> getContributors(Context context, String owner, String repo) {
        Realm realm = Realm.getInstance(context);
        return realm.where(ContributorModel.class)
                .equalTo("owner", owner)
                .equalTo("repo", repo)
                .findAll();
    }

    public static void saveNewContributors(Context context, List<Contributor> users, String owner, String repo) {
        Realm realm = Realm.getInstance(context);

        RealmResults<ContributorModel> models = getContributors(context, owner, repo);
        realm.beginTransaction();
        while (models.size() != 0)
            models.get(0).removeFromRealm();
        realm.commitTransaction();

        realm.beginTransaction();
        for (Contributor user : users) {
            ContributorModel model = new ContributorModel(user.getLogin(), owner, repo, user.getContributions());
            realm.copyToRealm(model);
        }
        realm.commitTransaction();
    }

}
