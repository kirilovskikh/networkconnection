package com.kirilovskikh.rxasync.api.network;

import com.kirilovskikh.rxasync.api.network.responseModel.Contributor;

import java.util.List;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class ContributorEvent {

    private List<Contributor> list;

    public ContributorEvent(List<Contributor> list) {
        this.list = list;
    }

    public List<Contributor> getList() {
        return list;
    }

}
