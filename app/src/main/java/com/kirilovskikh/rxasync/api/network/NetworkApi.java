package com.kirilovskikh.rxasync.api.network;

import android.content.Context;

import com.kirilovskikh.rxasync.api.network.responseModel.Contributor;

import java.util.List;

import retrofit.RestAdapter;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kirilovskikh on 22.01.15.
 */
public class NetworkApi {

    private static final String URL = "https://api.github.com";
    private GithubApiService service;

    public NetworkApi() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(URL)
                .build();
        service = restAdapter.create(GithubApiService.class);
    }

    public Observable<List<Contributor>> rxGetUsers(final Context context, final String owner, final String repo) {
        return Observable.create(new Observable.OnSubscribe<List<Contributor>>() {
            @Override
            public void call(Subscriber<? super List<Contributor>> subscriber) {
                List<Contributor> contributors = service.getContributors(owner, repo);

                subscriber.onNext(contributors);
                subscriber.onCompleted();
                return;
            }
        }).observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.newThread());
    }

    public List<Contributor> getUsers(Context context, String owner, String repo) {
        return service.getContributors(owner, repo);
    }

}
