package com.kirilovskikh.rxasync.api.network;

import com.kirilovskikh.rxasync.api.network.responseModel.Contributor;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by kirilovskikh on 22.01.15.
 */
public interface GithubApiService {

    @GET("/repos/{owner}/{repo}/contributors")
    List<Contributor> getContributors(
            @Path("owner") String owner,
            @Path("repo") String repo
    );

}
