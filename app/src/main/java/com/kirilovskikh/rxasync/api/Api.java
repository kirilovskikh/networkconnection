package com.kirilovskikh.rxasync.api;

import android.content.Context;

import com.kirilovskikh.rxasync.api.db.RealmApi;
import com.kirilovskikh.rxasync.api.db.model.ContributorModel;
import com.kirilovskikh.rxasync.api.network.ContributorEvent;
import com.kirilovskikh.rxasync.api.network.NetworkApi;
import com.kirilovskikh.rxasync.api.network.responseModel.Contributor;
import com.kirilovskikh.rxasync.utils.PreferencesConstant;
import com.kirilovskikh.rxasync.utils.PreferencesManager;

import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class Api {

    private static final long TIME_CACHE = 1000 * 60 * 12;

    public static Observable<RealmResults<ContributorModel>> getContributorList(final Context context, final String owner, final String repo) {
        return Observable.create(new Observable.OnSubscribe<RealmResults<ContributorModel>>() {
            @Override
            public void call(Subscriber<? super RealmResults<ContributorModel>> subscriber) {

                long currentTime = new Date().getTime();

                if (currentTime - PreferencesManager.getInstance(context).getLongValue(PreferencesConstant.CONTRIBUTOR_LAST_CACHE) > TIME_CACHE) {
                    NetworkApi api = new NetworkApi();
                    List<Contributor> users = api.getUsers(context, owner, repo);

                    RealmApi.saveNewContributors(context, users, owner, repo);
                    PreferencesManager.getInstance(context).setLongKey(PreferencesConstant.CONTRIBUTOR_LAST_CACHE, currentTime);
                }

                subscriber.onNext(null);
                subscriber.onCompleted();
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread());
    }

    public static void fetchContributorList (final Context context, final String owner, final String repo) {
        NetworkApi api = new NetworkApi();
        api.rxGetUsers(context, owner, repo).subscribe(new Action1<List<Contributor>>() {
            @Override
            public void call(List<Contributor> contributors) {
                EventBus eventBus = EventBus.getDefault();
                eventBus.post(new ContributorEvent(contributors));
            }
        });
    }

}