package com.kirilovskikh.rxasync.api.db.model;

import io.realm.RealmObject;

/**
 * Created by kirilovskikh on 05.02.15.
 */
public class ContributorModel extends RealmObject {

    private String login;
    private String owner;
    private String repo;
    private int contributions;

    public ContributorModel() {
    }

    public ContributorModel(String login, String owner, String repo, int contributions) {
        this.login = login;
        this.owner = owner;
        this.repo = repo;
        this.contributions = contributions;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getContributions() {
        return contributions;
    }

    public void setContributions(int contributions) {
        this.contributions = contributions;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRepo() {
        return repo;
    }

    public void setRepo(String repo) {
        this.repo = repo;
    }

}
