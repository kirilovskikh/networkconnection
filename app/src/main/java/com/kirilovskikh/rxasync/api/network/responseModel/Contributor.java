package com.kirilovskikh.rxasync.api.network.responseModel;

/**
 * Created by kirilovskikh on 22.01.15.
 */
public class Contributor {

    private String login;
    private int contributions;

    public Contributor() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getContributions() {
        return contributions;
    }

    public void setContributions(int contributions) {
        this.contributions = contributions;
    }

}
